import json


def serialize(data_dict):
    return json.dumps(data_dict)


def deserialize(json_obj):
    return json.loads(json_obj)
