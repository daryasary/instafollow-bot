sign = {0: "$", 1: "#"}
status = {0: "error", 1: "success"}
sender = {1: "APP", 2: "INSTAGRAM", 3: "INSTAFOLLOW"}


def task_logger(msg, msg_type):
    message = '{sign}{sender} ({status}): {msg} '.format(
        sign=sign[msg_type % 10],
        status=status[msg_type % 10],
        sender=sender[msg_type // 10], msg=msg
    )
    print(message, flush=True)
