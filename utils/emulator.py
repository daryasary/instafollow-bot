import json
import random

import requests
import time

from account.models import AccountLog
from utils.logger import task_logger
from utils.proxy import set_proxy
from utils.serializers import serialize, deserialize
from utils.statics import accept_language, user_agent, instagram_url_login, \
    default_headers, instagram_url_user_info, instafollow_login_url, \
    instafollow_fetch_orders, \
    instafollow_submit_action_list, instagram_url, instagram_delay, \
    instafollow_token


class Emulator(object):
    def __init__(self, user):
        self.user = user
        self.proxy = set_proxy()
        self.session = self.set_session()
        self.instagram = False
        self.user_id = None
        self.login_instagram()
        self.token = self.get_instafollow_token()
        self.total = 0

    @property
    def credential(self):
        return {'username': self.user.username,
                'password': self.user.password}

    @property
    def instafollow_header(self):
        return dict(authorization='JWT {token}'.format(token=self.token))

    @property
    def valid(self):
        # TODO:Validate class instance before actions stat
        return bool(self.instagram and self.user_id and self.token)

    @staticmethod
    def generate_header(**kwargs):
        return {**default_headers, **kwargs}

    def set_session(self):
        session = requests.Session()
        session.cookies.update({'sessionid': '', 'mid': '', 'ig_pr': '1',
                                'ig_vw': '1920', 'csrftoken': '',
                                's_network': '', 'ds_user_id': ''})
        session.headers.update({'Accept-Encoding': 'gzip, deflate',
                                'Accept-Language': accept_language,
                                'Connection': 'keep-alive',
                                'Content-Length': '0',
                                'Host': 'www.instagram.com',
                                'Origin': 'https://www.instagram.com',
                                'Referer': 'https://www.instagram.com/',
                                'User-Agent': user_agent,
                                'X-Instagram-AJAX': '1',
                                'X-Requested-With': 'XMLHttpRequest'})
        task_logger('Proxy assigned: {}'.format(self.proxy), 11)
        session.proxies.update(self.proxy)
        return session

    def login_instagram(self):
        try:
            r = self.session.get(instagram_url)
        except requests.exceptions.ProxyError:
            task_logger('Proxy error, renewing proxy...', 10)
            self.proxy = set_proxy()
            self.session.proxies.update(self.proxy)
            return self.login_instagram()

        self.session.headers.update({'X-CSRFToken': r.cookies.get('csrftoken')})
        time.sleep(5 * random.random())
        _login = self.session.post(instagram_url_login, data=self.credential,
                                   allow_redirects=True)
        self.session.headers.update(
            {'X-CSRFToken': _login.cookies.get('csrftoken')})
        self.csrftoken = _login.cookies.get('csrftoken')
        time.sleep(5 * random.random())

        if _login.status_code == 200:
            task_logger('Connected successfully to instagram {}'.format(
                self.user.username), 11)
            r = self.session.get('https://www.instagram.com/')
            finder = r.text.find(self.credential['username'])
            if finder != -1:
                task_logger('Instagram login was successful for {}'.format(
                    self.user.username), 21)
                self.user_id = self.get_instagram_user_id(
                    self.credential['username']
                )
                self.instagram = True
                self.user.available = True
                self.user.save()
        else:
            task_logger('Instagram login failed for {}'.format(
                self.user.username), 20)
            self.user.available = False
            self.user.save()

    def get_instagram_user_id(self, username):
        url_info = instagram_url_user_info % (username)
        info = self.session.get(url_info)
        all_data = json.loads(info.text)
        id_user = all_data['user']['id']
        return id_user

    def get_instafollow_token(self):
        if not self.instagram:
            self.login_instagram()

        payload = serialize(
            {"ds_user_id": self.user_id, 'token': instafollow_token}
        )

        response = requests.post(
            instafollow_login_url, data=payload, headers=default_headers

        )

        if response.status_code != requests.codes.ok:
            task_logger('Cannot get token from instafollow for {}'.format(
                self.user.username), 30)
            # TODO: Register user to Instafollow
            self.user.message = 'User is not registered in instafollow'
            self.user.available = False
            self.user.save()
            task_logger(
                'Instafollow token failed for {}'.format(self.user.username),
                10
            )
            return None

        task_logger(
            'Instafollow token assigned to {}'.format(self.user.username), 11
        )
        return deserialize(response.text)['token'] or None

    def do_instagram_actions(self, targets):
        done = list()
        valid, invalid = 0, 0

        for i, target in enumerate(targets):
            # Like or Follow target on instagram by user session
            do = self.session.post(target['target']['action_url'])
            if do.status_code // 100 == 2:
                done.append(target['id'])
                task_logger('Action done in intagram #{}'.format(i), 21)
                valid += 1
                time.sleep(instagram_delay)

            else:
                task_logger(
                    'Instagram action error {} #{}'.format(do.status_code, i), 20
                )
                invalid += 1

        task_logger(
            '{} valid and {} invalid targets followed/liked'.format(
                valid,
                invalid
            ),
            11)
        # Free worker if account is blocked
        if not valid:
            return False

        return done

    def do_instafollow_actions(self, done):
        payload = {"orders": done, "order_type": 'F'}
        response = requests.post(
            instafollow_submit_action_list,
            data=serialize(payload),
            headers=self.generate_header(**self.instafollow_header))

        if response.status_code == '401':
            # Renew instafollow token
            task_logger('instafollow token expired', 30)
            self.get_instafollow_token()
            return self.do_instafollow_actions(done)

        result = response.status_code // 100 == 2
        body = deserialize(response.text)
        balance = body['balance'] if 'balance' in body.keys() else None
        if not result:
            # In case of bad request or out of service responses from server
            task_logger('instafollow is not reachable, will retry action...',
                        30)
            response = requests.post(
                instafollow_submit_action_list,
                data=serialize(payload),
                headers=self.generate_header(**self.instafollow_header)
            )
            result = response.status_code // 100 == 2
            if not result:
                self.user.message = 'Error from instafollow'
                self.user.available = False
                self.user.save()

        task_logger('Flow result was {} balance is {}'.format(result, balance),
                    11)
        log = AccountLog.objects.create(account=self.user, balance=balance,
                                        result=result, message=response.text)
        return log.result

    def do_actions(self, targets):
        if not isinstance(targets, list):
            return False

        done = self.do_instagram_actions(targets)

        if not done:
            self.user.message = 'User has been blocked by instagram'
            self.user.available = False
            self.user.save()
            return False

        result = self.do_instafollow_actions(done)

        return result

    def fetch_targets(self, like=False, follow=False):
        url = instafollow_fetch_orders

        if like:
            url += "like"
        if follow:
            url += "follow"

        response = requests.get(url, headers=self.instafollow_header)

        if response.status_code == requests.codes.ok:
            targets = deserialize(response.text)
            task_logger('{} Targets fetched'.format(len(targets)), 31)
            return targets

        elif response.status_code == '403':
            # Instafollow Token is expired
            task_logger('instafollow token expired', 30)
            self.get_instafollow_token()
            return self.fetch_targets(like, follow)

        return []

    def start_activity(self):
        targets = self.fetch_targets()
        actions = True

        while targets and actions:
            task_logger('Worker is trying new flow', 11)
            actions = self.do_actions(targets)
            targets = self.fetch_targets()

        task_logger('Worker is going to off !!', 10)
        return actions and targets

    @classmethod
    def start(cls, user):
        em = cls(user=user)
        if em.valid:
            error = em.start_activity()
            return error
        return False
