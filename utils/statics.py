accept_language = 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4'
user_agent = ("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 "
              "(KHTML, like Gecko) Chrome/48.0.2564.103 Safari/537.36")
default_headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
}
instagram_delay = 20
instagram_url = 'https://www.instagram.com/'
instagram_url_login = 'https://www.instagram.com/accounts/login/ajax/'
instagram_url_user_info = "https://www.instagram.com/%s/?__a=1"


instafollow_base_url = "http://insta.vasapi.click/"
# instafollow_base_url = "http://localhost:8001/"
instafollow_login_url = instafollow_base_url + "account/login/"
instafollow_register_url = instafollow_base_url + "account/register/"
instafollow_fetch_orders = instafollow_base_url + "orders/fetch/"
instafollow_submit_action_list = instafollow_base_url + "orders/submit_actions/"
instafollow_token = 'EK4G6TlJLzNoUx8q'
