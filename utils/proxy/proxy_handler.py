import csv
import json
import random

from utils.proxy.file_handler import prepare_path


def set_proxy(index=None):
    """Assign proxy to each user"""
    with open(prepare_path(), 'r') as proxies:
        proxies_list = json.loads(proxies.read())

    if not index:
        index = random.randint(0, len(proxies_list))

    return proxies_list[index % len(proxies_list)]


def parse_proxies_list(filename):
    """Call this function to renew proxy list files"""
    csv_path = prepare_path(filename)
    py_path = prepare_path()
    python_file = open(py_path, 'w')
    template = "{}://{}:{}"
    tmp = list()
    with open(csv_path, 'r') as proxies:
        reader = csv.reader(proxies, delimiter='\t')
        for row in reader:
            tmp.append({row[2].lower(): template.format(
                row[2].lower(), row[0], row[1])})
    python_file.write(json.dumps(tmp))
    python_file.close()
    return csv_path, py_path
