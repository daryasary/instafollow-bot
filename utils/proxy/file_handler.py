import os

from django.conf import settings


def prepare_path(filename='proxies_list.json'):
    """File and path handler for proxies file"""
    path = settings.PROXIES_FILE_PATH
    file_path = os.path.join(path, filename)
    return file_path
