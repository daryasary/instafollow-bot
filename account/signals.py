from django.db.models.signals import post_save
from django.dispatch import receiver

from account.models import Account
from account.tasks import activity_handler


@receiver(post_save, sender=Account)
def workers_moderator(sender, instance, created, **kwargs):
    """ Workers moderator is signal and will fire after each save or
    modification on Account.models table, at first moderator will
    check to find out redundant task running for this account, if
    task is in queue or active call .revoke and initiate it again"""

    if not instance.has_active_worker:
        activity_handler.delay(instance.id)
