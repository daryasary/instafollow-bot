from django.test import SimpleTestCase
from django.test import TestCase

from account.models import Account
from utils.emulator import Emulator
from utils.proxy import set_proxy


class AccountTestCase(TestCase):
    def setUp(self):
        Account.objects.create(username='ehsann4444', password='yara3310088413')
        Account.objects.create(username='ehsann444', password='yara3310088413')

    def test_emulator_initiating_phase(self):
        user = Account.objects.get(pk=1)
        em = Emulator(user=user)
        self.assertIsInstance(em, Emulator)
        self.assertTrue(em.valid)

    def test_instagram_and_instafollow_login(self):
        user = Account.objects.get(pk=1)
        em = Emulator(user=user)
        self.assertIsInstance(em, Emulator)
        self.assertTrue(user.available)
        self.assertTrue(em.valid)

    def test_bot_workers_instances(self):
        """Should run celery worker first"""
        accounts = Account.objects.all()
        for account in accounts:
            self.assertTrue(account.has_active_worker)


class ProxyTestCase(SimpleTestCase):
    def test_set_proxy_module(self):
        proxy = set_proxy()
        self.assertIsNotNone(proxy)
        self.assertIsInstance(proxy, str)
