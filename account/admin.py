from django.contrib import admin

from account.models import Account, AccountLog


class AccountAdmin(admin.ModelAdmin):
    actions = ('start_bot', )
    list_display = ['username', 'password', 'cookie',
                    'available', 'message', 'modified_at']

    def start_bot(self, request, queryset):
        for account in queryset:
            account.message = ''
            account.save()


class AccountLogAdmin(admin.ModelAdmin):
    list_display = ['account', 'balance', 'created_at', 'result', 'message']

admin.site.register(Account, AccountAdmin)
admin.site.register(AccountLog, AccountLogAdmin)
