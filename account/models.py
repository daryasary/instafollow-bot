from django.db import models
from django.utils.translation import ugettext_lazy as _
from celery.task.control import inspect


class Account(models.Model):
    username = models.CharField(verbose_name=_("Username"), max_length=64)
    password = models.CharField(verbose_name=_("Password"), max_length=64)
    cookie = models.TextField(blank=True, editable=False)

    available = models.BooleanField(default=False, editable=False)
    message = models.TextField(blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.username

    class Meta:
        ordering = ['-created_at']

    @property
    def has_active_worker(self):
        active_tasks = inspect().active()
        for worker in active_tasks:
            if active_tasks[worker]:
                for task in active_tasks[worker]:
                    if task['type'] == 'Activity Handler' and self.id in eval(
                            task['args']):
                        return True
        return False


class AccountLog(models.Model):
    account = models.ForeignKey(Account, related_name='logs')
    balance = models.IntegerField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    result = models.BooleanField(default=True, editable=False)
    message = models.TextField(blank=True)

    def __str__(self):
        return self.account.username

    class Meta:
        ordering = ['-created_at']
