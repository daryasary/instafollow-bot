from celery import shared_task
from celery.utils.log import get_task_logger

from account.models import Account
from utils.emulator import Emulator

logger = get_task_logger(__name__)


@shared_task(name='Activity Handler')
def activity_handler(aid):
    # TODO: Generate proper utils and update accounts
    account = Account.objects.get(pk=aid)
    emulator = Emulator.start(account)
    return emulator
