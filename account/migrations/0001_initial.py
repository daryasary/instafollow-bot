# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('username', models.CharField(max_length=64, verbose_name='Username')),
                ('password', models.CharField(max_length=64, verbose_name='Password')),
                ('cookie', models.TextField(blank=True, editable=False)),
                ('available', models.BooleanField(default=False, editable=False)),
                ('message', models.TextField(blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['-created_at'],
            },
        ),
        migrations.CreateModel(
            name='AccountLog',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('balance', models.IntegerField(null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('result', models.BooleanField(default=True, editable=False)),
                ('message', models.TextField(blank=True)),
                ('account', models.ForeignKey(to='account.Account', related_name='logs')),
            ],
            options={
                'ordering': ['-created_at'],
            },
        ),
    ]
